#ifndef ENTRYFORM_H
#define ENTRYFORM_H

#include <QWidget>

namespace Ui {
class EntryForm;
}

class EntryForm : public QWidget
{
    Q_OBJECT
public slots:
    void okClicked();
    void cancelClicked();
    //void testRandomClicked();
public:
    explicit EntryForm(QWidget *parent = Q_NULLPTR);
    ~EntryForm();

private:
    Ui::EntryForm *ui;
};

#endif // ENTRYFORM_H
