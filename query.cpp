#include "query.h"
#include <QString>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlRecord>
#include <QSettings>
#include <QRandomGenerator>
#include <QDebug>
#include <QString>

Query::Query()
{
     //createVehicleTable();
     //createMileageTable();
}

void Query::createTables()
{
     createVehicleTable();
     createVehicleChangeTable();
     createMileageTable();
     createMileageChangeTable();

    //lifetimeStuff = new QList<QVariant>();
}

Query::~Query()
{
    //delete lifetimeStuff;
}

void Query::createVehicleTable()
{
     QString queryString = "";
     QSettings settings;
     QString type = settings.value("config/databasetype").toString();
     if(type == "sqlite")
     {
          queryString = "create table if not exists vehicles \
                    (id integer primary key on conflict fail , \
                     description text)";
     }
     else if(type =="mariadb")
     {
          queryString = "create table if not exists vehicles (\
                    id int(11) not null , \
                    description varchar(255) default null, \
                    primary key (id) \
                    ) engine=innodb default charset=latin1 \
                    ";
     }
     QSqlDatabase db = QSqlDatabase::database(type);
     if (db.isOpen())
     {
          QSqlQuery query(queryString,db);
          if (!query.isActive())
          {
               qDebug()<<"create vehicle table failed:"<<"dbase type:"<<type<<":"<<query.lastError().text()
                      <<query.lastQuery();
               errorMessage = "createVehicleTable query problem: " + query.lastError().text();
          }
     }
     else
     {
          errorMessage = "createVehicleTable db problem: " + db.lastError().text();
     }
}

void Query::createMileageTable()
{
     QString queryString = "";
     QSettings settings;
     QString type = settings.value("config/databasetype").toString();
     if(type == "sqlite")
     {
          queryString = "create table if not exists fuel_mileage \
                    (id integer primary key on conflict fail, \
                     miles real, \
                     gallons real, \
                     cost real, \
                     fillup_date text, \
                     vehicle_id integer not null, \
                     foreign key (vehicle_id) references vehicle(id) \
                     )";
     }
     else if(type =="mariadb")
     {
          queryString = "create table if not exists fuel_mileage (\
                    id int(11) not null , \
                    miles double default null, \
                    gallons double default null, \
                    cost double default null, \
                    fillup_date date default null, \
                    vehicle_id int(11) default null, \
                    primary key (id), \
                    key vehicle_id_fk (vehicle_id), \
                    constraint vehicle_id_fk foreign key (vehicle_id) \
                    references vehicles(id) on delete no action on update no action )\
                    engine=innodb default charset=latin1 \
                    ";
     }
     QSqlDatabase db = QSqlDatabase::database(type);
     if (db.isOpen())
     {
          QSqlQuery query(queryString,db);
          if (!query.isActive())
          {
               qDebug()<<"create fuel_mileage table failed:"<<query.lastError().text()
                      <<query.lastQuery();
               errorMessage = "createMileageTable query problem: " + query.lastError().text();
          }
     }
     else
     {
          errorMessage = "createMileageTable db problem: " + db.lastError().text();
     }
}

void Query::createVehicleChangeTable()
{
     QString queryString = "";
     QSettings settings;
     QString type = settings.value("config/databasetype").toString();
     if(type == "sqlite")
     {
          queryString = "create table if not exists vehicle_changes \
                    ( \
                         vehicle_id int unique, \
                         foreign key (vehicle_id) references vehicles(id) on delete cascade on update cascade\
                         )";
     }
     else if(type =="mariadb")
     {
          queryString = " CREATE TABLE if not exists `vehicle_changes` ( \
                    `vehicle_id` int(11) DEFAULT NULL, \
                    unique key (vehicle_id), \
                    CONSTRAINT `vehicle_changes_ibfk_1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE \
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
     }
     QSqlDatabase db = QSqlDatabase::database(type);
     if (db.isOpen())
     {
          QSqlQuery query(queryString,db);
          if (!query.isActive())
          {
               errorMessage = "createVehicleChangeTable query problem: " + query.lastError().text();
          }
     }
     else
     {
          errorMessage = "createVehicleChangeTable db problem: " + db.lastError().text();
     }
}

void Query::createMileageChangeTable()
{
     QString queryString = "";
     QSettings settings;
     QString type = settings.value("config/databasetype").toString();
     if(type == "sqlite")
     {
          queryString = "create table if not exists fuel_mileage_changes  \
                    ( \
                         fm_id int unique, \
                         foreign key (fm_id) references fuel_milage(id) on update cascade on delete cascade \
                         )";
     }
     else if(type =="mariadb")
     {
          queryString = "create table if not exists fuel_mileage_changes \
                    ( \
                         fm_id int,  \
                         unique key (fm_id), \
                         constraint fm_id_fk foreign key (fm_id) references fuel_mileage(id) on delete cascade on update cascade \
                         ) ENGINE=InnoDB DEFAULT CHARSET=latin1 \
                    ";
     }
     QSqlDatabase db = QSqlDatabase::database(type);
     if (db.isOpen())
     {
          QSqlQuery query(queryString,db);
          if (!query.isActive())
          {
               errorMessage = "createFuelMileageChangeTable query problem: " + query.lastError().text();
          }
     }
     else
     {
          errorMessage = "createFuelMileageChangeTable db problem: " + db.lastError().text();
     }
}

int Query::getNumber()
{
     QRandomGenerator *gen = QRandomGenerator::system();
     int number = gen->bounded(1000000000);  //1,000,000,000, 1 billion
     return number;
}


bool Query::insertVehicle(QString n)
{
     bool ok = false;
     QSettings settings;
     QString type = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(type);

     QString str = "";
     //get a number and make sure it isn't there
     QString goodnumber = "no";
     int number = getNumber();
     int howmanytimes = 0;
     if (db.isOpen())
     {
          do
          {
               str = "select count(*) from vehicles where id = :id";
               QSqlQuery q(str,db);
               q.prepare(str);
               q.bindValue(":id",number);
               q.exec();
               while(q.next())
               {
                    int count = q.value(0).toInt();
                    if (count ==0)
                    {
                         goodnumber = "yes";
                    }
                    else
                    {
                         number = getNumber();
                    }
               }
               if (howmanytimes == 10)
               {
                    goodnumber = "yes";
                    number = -1;
               }
               howmanytimes++;
          }while(goodnumber == "no");

          str = "insert into vehicles (id,description) values (:id,:description)";
          db.transaction(); //start a transaction
          QSqlQuery q(str, db);
          q.prepare(str);
          q.bindValue(":id",number);
          q.bindValue(":description",n);
          q.exec();

          //now we need to insert a record into the vehicle changes table
          str = "insert into vehicle_changes(vehicle_id) values (:id)";
          q.prepare(str);
          q.bindValue(":id",number);
          q.exec();
          if (db.commit())
          {
               ok = true;
          }
          else
          {
               //qDebug()<<q.lastError().text();
               errorMessage = "insertVehicle transaction problem: " + db.lastError().text();
          }
     }
     else
     {
          errorMessage = "insertVehicle db not open problem: " + db.lastError().text();
     }
     return ok;
}

void Query::updateVehicle(Vehicle v)
{
     QString queryString = "update vehicles set description = :desc where id = :id";
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);
     if (db.isOpen())
     {
          db.transaction();
          QSqlQuery q(db);
          q.prepare(queryString);
          q.bindValue(":desc",v.description);
          q.bindValue(":id",v.id);
          q.exec();

          //now we need to insert this vehicle id into the changes table
          queryString = "insert into vehicle_changes(vehicle_id) values (:id)";
          q.prepare(queryString);
          q.bindValue(":id",v.id);
          q.exec();
          if (!db.commit())
          {
               errorMessage = "updateVehicle transaction problem: " + db.lastError().text();
          }
     }
     else
     {
          qDebug()<<"inside the else in updateVehicle";
          errorMessage = "updateVehicle db problem: " + db.lastError().text();
     }
}

bool Query::selectVehicle(VehicleModel *model)
{
     bool ok = false;
     //empty out the model first
     model->removeRows(0,model->rowCount(QModelIndex()));
     QString s = "select id,description from vehicles";

     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);
     if (db.isOpen())
     {
          QSqlQuery q(s,db);
          q.prepare(s);
          if(q.exec())
          {
               ok = true;
               //need a couple colors to switch between
               QColor one = Qt::white;
               QColor two = Qt::lightGray;
               int i=0;
               while(q.next())
               {
                    Vehicle v;
                    v.id = q.value(0).toInt();
                    v.description = q.value(1).toString();

                    //alternate the row colors
                    if(i %2 == 0)
                    {
                         model->insertRow(model->rowCount(QModelIndex()),1,QModelIndex(),v,one);
                    }
                    else
                    {
                         model->insertRow(model->rowCount(QModelIndex()),1,QModelIndex(),v,two);
                    }

                    i++;
               }
          }
          else
          {
               //qDebug()<<"error trying to exec selectvehicle: "<<q.lastError().text();
               errorMessage = "selectVehicle query problem:  " + q.lastError().text();
          }
     }
     else
     {
          errorMessage = "selectVehicle db problem: " + db.lastError().text();
     }
     return ok;
}

bool Query::selectFuelMileage(int vehicleId, AllDataModel *model)
{
     bool ok = false;
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);

     QString s = "select id,fillup_date,miles,gallons,cost,miles/gallons as mpg \
               from fuel_mileage \
               where vehicle_id = :id \
               order by fillup_date desc ";

               QSqlQuery query(s,db);
     if (db.isOpen())
     {
          query.prepare(s);
          query.bindValue(":id",vehicleId);

          //qDebug()<<"row count:"<<model->rowCount(QModelIndex());
          if (query.exec())
          {
               ok = true;

               //need a couple colors to switch between
               QColor one = Qt::white;
               //QColor two = Qt::lightGray;
               QColor two(215, 238, 247);
               int whichColor=0;

               //hold the old month and year for comparison
               int oldmonth = 0;
               int oldyear = 0;

               //get the first row
               if (query.next())
               {
                    Mileage f;
                    f.id = query.value(0).toInt();
                    f.date = QDate::fromString(query.value(1).toString(), "yyyy-MM-dd");
                    f.miles = query.value(2).toDouble();
                    f.gallons = query.value(3).toDouble();
                    f.cost = query.value(4).toDouble();
                    f.mpg = query.value(5).toDouble();

                    oldmonth = f.date.month();
                    oldyear = f.date.year();

                    model->insertRow(model->rowCount(QModelIndex()),1,QModelIndex(),f);
                    model->addColor(one);
               }
               while(query.next())
               {
                    Mileage m;
                    m.id = query.value(0).toInt();
                    m.date = QDate::fromString(query.value(1).toString(), "yyyy-MM-dd");
                    m.miles = query.value(2).toDouble();
                    m.gallons = query.value(3).toDouble();
                    m.cost = query.value(4).toDouble();
                    m.mpg = query.value(5).toDouble();

                    //qDebug()<<m.id<<m.miles<<m.gallons<<m.cost<<m.date<<m.mpg;
                    model->insertRow(model->rowCount(QModelIndex()),1,QModelIndex(),m);

                    //figure out what color this row is supposed to be
                    if (oldmonth == m.date.month() && oldyear == m.date.year())
                    {
                         if(whichColor == 0)
                              model->addColor(one);
                         else
                              model->addColor(two);
                    }
                    else
                    {
                         if(whichColor == 1)
                         {
                              whichColor = 0;
                              model->addColor(one);
                         }
                         else
                         {
                              whichColor = 1;
                              model->addColor(two);
                         }
                    }
                    //keep the month and year for the next iteration
                    oldmonth = m.date.month();
                    oldyear = m.date.year();
               }//end while
          }
          else
          {
               //qDebug()<<"problemhere:"<<query.lastError().text();
               errorMessage = "selectFuelMileage query problem: " + query.lastError().text();
          }
     }
     else
     {
          errorMessage = "selectFuelMileage db problem: " + db.lastError().text();
     }
     return ok;
}

QList< QList<QVariant> > Query::getExportData(int id)
{
     QList< QList<QVariant>> everything;
     QString q = "";
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);

     q = "select description,miles,gallons,cost,fillup_date \
               from vehicles \
               \
               join fuel_mileage \
               on vehicle_id = vehicles.id \
               \
               where vehicles.id = :id \
               ";

               QSqlQuery query(q,db);

     if (db.isOpen())
     {
          query.prepare(q);
          query.bindValue(":id",id);
          if (query.exec())
          {
               while(query.next())
               {
                    QList<QVariant> stuff;
                    QSqlRecord r = query.record();
                    stuff.append(r.value(0));   //description
                    stuff.append(r.value(1));   //miles
                    stuff.append(r.value(2));   //gallons
                    stuff.append(r.value(3));   //cost
                    stuff.append(r.value(4));   //fillup_date

                    //put this little qlist onto the big qlist
                    everything.append(stuff);
               }//end while loop
          }
     }
     else
     {

     }
     return everything;
}

QString Query::getVehicleDescription(int i)
{
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);

     QString desc = "";
     QString s = "select description from vehicles where id = :id";
     QSqlQuery q(s,db);
     if (db.isOpen())
     {
          //qDebug()<<"i'm guessing theres no database connection here";
          q.prepare(s);
          q.bindValue(":id",i);
          if (q.exec())
          {
               if (q.next())
               {
                    desc = q.value(0).toString();
               }
          }
          else
          {
               errorMessage = "getVehicleDescription query problem: " + q.lastError().text();
               //QString error = q.lastError().text();
               //QMessageBox message(QMessageBox::Critical,"Problem!",error,QMessageBox::Ok,this,Qt::Dialog);
               //message.exec();
          }
     }
     else
     {
          errorMessage = "getVehicleDescription db problem: " + db.lastError().text();
     }
     return desc;
}


bool Query::insertFuelMileage(int id,double miles,double gallons,double cost,QString date)
{
     bool ok = false;
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);

     QString s = "";
     QString goodnumber = "no";
     int number = getNumber();
     //qDebug()<<"first number: "<< QString::number(number);
     int howmanytimes = 0;
     if (db.isOpen())
     {
          do
          {
               s = "select count(*) from fuel_mileage where id = :id";
               QSqlQuery q(s,db);
               q.prepare(s);
               q.bindValue(":id",number);
               q.exec();
               while(q.next())
               {
                    int count = q.value(0).toInt();
                    //qDebug()<<"is this number in there?: " << QString::number(count);
                    if (count == 0)
                    {
                         goodnumber = "yes";
                    }
                    else
                    {
                         number = getNumber();
                    }
               }
               if (howmanytimes == 10)
               {
                    goodnumber = "yes";
                    number = -1;
               }
               howmanytimes++;
               //qDebug()<<"good number is " << goodnumber;
          }while(goodnumber == "no");

          s = "insert into fuel_mileage (id,miles,gallons,cost,fillup_date,vehicle_id)\
                    values(:id,:miles,:gallons,:cost,:date,:vid) ";

          db.transaction();
          QSqlQuery query(s,db);
          query.prepare(s);
          query.bindValue(":id",number);
          query.bindValue(":miles",miles);
          query.bindValue(":gallons",gallons);
          query.bindValue(":cost",cost);
          query.bindValue(":date",date);
          query.bindValue(":vid",id);
          query.exec();

          //insert this record's id into the mileage change table
          s = "insert into fuel_mileage_changes (fm_id) values (:id)";
          query.prepare(s);
          query.bindValue(":id",number);
          query.exec();
          if(db.commit())
          {
               ok = true;
          }
          else
          {
               errorMessage = "insertFuelMileage query problem: " + query.lastError().text();
               //qDebug()<<"insertFuelMileage:"<<query.lastError().text();
               //        QString error = "insertFuelMileage\n"+query.lastError().text();
               //        QMessageBox message(QMessageBox::Critical,"Problem!",error,QMessageBox::Ok,(QWidget*)owner,Qt::Dialog);
               //        message.exec();
               //QMessageBox::warning(owner,"getVehidledesc","something bad",QMessageBox::Ok);
          }
     }
     else
     {
          errorMessage = "insertFuelMileage db problem: " + db.lastError().text();
     }
     //qDebug()<<"error message: " << errorMessage;
     return ok;
}

bool Query::updateFuelMileage(QList<Mileage>t)
{
     bool ok = true;

     QString q = "update fuel_mileage set  \
               miles = :miles, \
               gallons = :gallons, \
               cost = :cost, \
               fillup_date = :fillup_date \
               where id = :id \
               ";
     QString str_changeq = "insert ignore into fuel_mileage_changes (fm_id) values (:fm_id)";
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);
     if (db.isOpen())
     {
          errorMessage = "";
          db.transaction();
          QSqlQuery query(db);
          QSqlQuery changeq(db);
          query.prepare(q);
          changeq.prepare(str_changeq);
          for(int i=0;i<t.size();i++)
          {
               auto m = t.at(i);
               //qDebug()<<"id:"<<m.id<<" miles:"<<m.miles<<" gallons:"<<m.gallons;
               query.bindValue(":id",m.id);
               query.bindValue(":miles",m.miles);
               query.bindValue(":gallons",m.gallons);
               query.bindValue(":cost",m.cost);
               query.bindValue(":fillup_date",m.date.toString("yyyy-MM-dd"));
               bool update_ok = query.exec();

               changeq.bindValue(":fm_id",m.id);
               bool insert_ok = changeq.exec();

               if (!update_ok)
               {
                    errorMessage += "updateFuelMileage query problem: " + query.lastError().text();
                    ok = false;
                    break;
               }
               if (!insert_ok)
               {
                    errorMessage += "updateFuelMileage query problem: " + changeq.lastError().text();
                    ok = false;
                    break;
               }
          }
          if (ok)
          {
              if (!db.commit())
              {
                   errorMessage += "updateFuelMileage query problem: " + db.lastError().text();
              }
              else
              {
                    db.rollback();
                    //undo the changes to the model
              }
          }
     }
     else
     {
          //qDebug()<<"something wrong in updateFuelMileage";
          errorMessage = "updateFuelMileage db problem: " + db.lastError().text();
     }
     return ok;
}

QList<QVariant> Query::lifetimeStats(int id)
//bool Query::lifetimeStats(int id)
{
     QList<QVariant> stuff;
     QString q = "select sum(miles), \
               sum(gallons), \
               sum(cost) \
               from fuel_mileage \
               where vehicle_id = :id";
               QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);
     QSqlQuery query(q,db);

     if (db.isOpen())
     {
          query.prepare(q);
          query.bindValue(":id",id);
          if (query.exec())
          {
               query.next();
               QSqlRecord record = query.record();
               for (int i=0; i<record.count();i++)
               {
                    stuff.append(record.value(i));
                    //qDebug()<<record.value(i);
               }

               if (stuff.at(1).toDouble() !=0.0)
                    stuff.append(stuff.at(0).toDouble()/stuff.at(1).toDouble());  //mpg
               else
                    stuff.append("0");

               //miles,gallons,cost,mpg
          }
          else
               qDebug()<<"lifetimeStats: "<<query.lastError().text();

          q = "select count(*)\
                    from fuel_mileage \
                    where vehicle_id = :id";
                    query.prepare(q);
          query.bindValue(":id",id);
          if (query.exec())
          {
               query.next();
               stuff.prepend(query.value(0));
          }
     }
     else
     {
          qDebug()<<"lifetimeStats:  "<<db.lastError().text();
          errorMessage = "lifetimeStats db problem:" + db.lastError().text();
     }
     //    for(int i=0;i<stuff.size();i++)
     //    {
     //        qDebug()<<"end lifetimeStats loop"<<stuff.at(i);
     //    }

     //fillups,miles,gallons,cost,mpg
     return stuff;

}


QList< QList<QVariant> > Query::yearlyStats(int id)
{
     QList< QList<QVariant>> everything;
     QString q = "";
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);

     if (dbaseType == "sqlite")
     {
          q = "select strftime(\"%Y\",fillup_date) tyear,\
                    count(id) ,\
                    sum(miles) Miles,\
                    sum(gallons) Gallons,\
                    sum(cost) Cost\
                    from fuel_mileage\
                    where vehicle_id = :id\
                    and strftime(\"%Y\",fillup_date) != '0000'\
                                  group by tyear\
                                  order by tyear desc";
                                  //qDebug()<<q;
     }
     else if (dbaseType == "mariadb")
     {
          q = "select year(fillup_date) tyear,\
                    count(id) ,\
                    sum(miles) Miles,\
                    sum(gallons) Gallons,\
                    sum(cost) Cost\
                    from fuel_mileage\
                    where vehicle_id = :id\
                    and year(fillup_date) != '0000'\
                    group by tyear\
                    order by tyear desc";
                    //qDebug()<<q;
     }

     QSqlQuery query(q,db);

     if (db.isOpen())
     {
          query.prepare(q);
          query.bindValue(":id",id);
          //qDebug()<<query.executedQuery();
          if (query.exec())
          {
               while(query.next())
               {
                    QList<QVariant> stuff;
                    QSqlRecord r = query.record();
                    stuff.append(r.value(0));   //year
                    stuff.append(r.value(1));   //fillups
                    stuff.append(r.value(2));   //miles
                    stuff.append(r.value(3));   //gallons
                    stuff.append(r.value(4));   //cost

                    if (stuff.at(3).toDouble()!= 0.0)
                         stuff.append(stuff.at(2).toDouble()/stuff.at(3).toDouble());
                    else
                         stuff.append("0");

                    //put this little qlist onto the big qlist
                    everything.append(stuff);
               }//end while loop
          }//query executed successfully
          else
          {
               errorMessage = "yearlyStats query problem: " + query.lastError().text();
          }
     }//end db is open
     else
     {
          errorMessage = "yearlyStats db problem: " + db.lastError().text();
     }
     //qDebug()<<"query yearlystats at the end";
     //qDebug()<<"num records:"<<everything.count();
     //year,fillups,miles,gallons,cost,mpg
     return everything;
}

QList<QVariant> Query::getDistinctYears(int id)
{
     QList<QVariant>stuff;
     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);

     QString q = "";
     if (dbaseType == "sqlite")
     {
          q = "select distinct(strftime(\"%Y\",fillup_date)) theyear \
                    from fuel_mileage \
                    where vehicle_id = :id \
                    and strftime(\"%Y\",fillup_date) != '0000' \
                                  order by theyear desc";
     }
     else if (dbaseType == "mariadb")
     {
          q = "select distinct(year(fillup_date)) theyear \
                    from fuel_mileage \
                    where vehicle_id = :id \
                    and year(fillup_date) != '0000' \
                    order by theyear desc";
     }


     QSqlQuery query(q,db);

     if (db.isOpen())
     {
          query.prepare(q);
          query.bindValue(":id",id);
          if (query.exec())
          {
               while(query.next())
               {
                    stuff.append(query.record().value(0));
               }
          }
     }
     return stuff;
}

QList<QVariant> Query::monthlyStats(int id,QVariant year,QVariant month)
{
     QList<QVariant>stuff;

     QSettings settings;
     QString dbaseType = settings.value("config/databasetype").toString();
     QSqlDatabase db = QSqlDatabase::database(dbaseType);

     QString q = "";
     if(dbaseType == "sqlite")
     {
          q = "select count(id),sum(miles),sum(gallons),sum(cost) from fuel_mileage where vehicle_id = :id \
                    and strftime(\"%Y\",fillup_date) = :year and strftime(\"%m\",fillup_date) = :month ";
     }
     else if (dbaseType == "mariadb")
     {
          q = "select count(id),sum(miles),sum(gallons),sum(cost) from fuel_mileage where vehicle_id = :id \
                    and year(fillup_date) = :year and month(fillup_date) = :month ";

     }

     QSqlQuery query(q,db);

     if (db.isOpen())
     {
          query.prepare(q);
          query.bindValue(":id",id);
          query.bindValue(":year",year.toString());
          query.bindValue(":month",month.toString());
          if(query.exec())
          {
               while(query.next())
               {
                    QSqlRecord r = query.record();
                    int howmany = r.value(0).toInt();
                    if (howmany != 0)   //they have fillups
                    {
                         stuff.append(r.value(0));   //fillups
                         stuff.append(r.value(1));   //miles
                         stuff.append(r.value(2));   //gallons
                         stuff.append(r.value(3));   //cost
                         if (r.value(2).toDouble() != 0.0)
                              stuff.append(r.value(1).toDouble()/r.value(2).toDouble());  //mpg
                         else
                              stuff.append("0");  //mpg
                    }
                    else    //otherwise, nothing for this month/year combo
                    {
                         stuff.append("0");  //fillups
                         stuff.append("0");  //miles
                         stuff.append("0");  //gallons
                         stuff.append("0");  //cost
                         stuff.append("0");  //mpg
                    }
               }
          }//end exec
     }//end db is open
     return stuff;
}

QList< QList<QVariant> > Query::selectAllData()
{
    QSettings settings;
    QString dbaseType = settings.value("config/databasetype").toString();
    QSqlDatabase db = QSqlDatabase::database(dbaseType);

    int id = settings.value("config/vehicle").toInt();
    QString q = "select miles,gallons,cost,fillup_date,vehicle_id \
    from fuel_mileage \
    where vehicle_id = :id";

    QList< QList<QVariant> > everything;
    if (db.isOpen())
    {
        QSqlQuery query(db);
        query.prepare(q);
        query.bindValue(":id",id);
        if(query.exec())
        {
            int getColNames = 0;
            while(query.next())
            {
                QSqlRecord record = query.record();
                QList<QVariant> singleRow;
                int howmany = record.count();
                if (getColNames == 0)
                {
                    getColNames = 1;
                    for(int i=0;i<howmany;i++)
                    {
                        singleRow.append(record.fieldName(i));
                        qDebug()<<record.fieldName(i);
                    }
                    everything.append(singleRow);
                    singleRow.clear();
                }
                for(int i=0;i<howmany;i++)
                {
                    singleRow.append(record.value(i));
                    //qDebug()<<record.value(i);
                }
                everything.append(singleRow);
            }//end while
        }//if the query executed successfully
    }//if the database connection is open
    return everything;
}//end selectAllData

QString Query::error()
{
     return errorMessage;
}
