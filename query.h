#ifndef QUERY_H
#define QUERY_H

#include "vehiclemodel.h"
#include "alldatamodel.h"

class Query
{
public:
     Query();
    ~Query();

     void createTables();

     bool insertVehicle(QString);
     bool selectVehicle(VehicleModel*);
     void updateVehicle(Vehicle);

     bool updateFuelMileage(QList<Mileage>t);

     QString getVehicleDescription(int);

     bool selectFuelMileage(int, AllDataModel *);
     bool insertFuelMileage(int,double,double,double,QString);

     QList<QVariant> lifetimeStats(int);
     QList< QList<QVariant> > yearlyStats(int);
     //bool lifetimeStats(int);

     QList< QList<QVariant> > selectAllData();

     QList<QVariant> getDistinctYears(int);
     QList<QVariant> monthlyStats(int,QVariant,QVariant);//vehicleId,years.at(i),i.key());

    //monthlyStats(int);
     QList< QList<QVariant> > getExportData(int vehicle_id);
     QString error();

    //QList<QVariant>* getLifetimeStuff();

     int getNumber();

private:
    QString errorMessage;
    //QList<QVariant> *lifetimeStuff;

    //@here put this back later.
    //int getNumber();

    void createVehicleTable();
    void createMileageTable();
    void createVehicleChangeTable();
    void createMileageChangeTable();


};


#endif //QUERY_H
