#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include <QString>

namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = Q_NULLPTR);
    ~Login();

    QString getUsername();
    QString getPassword();

private:
    Ui::Login *ui;
};

#endif // LOGIN_H
